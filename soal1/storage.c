#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main() {
    // Download dataset
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");

    // // Extract dataset from the downloaded zip file
    system("unzip fifa-player-stats-database.zip");

    // // Cleanup: remove the downloaded zip file
    system("rm fifa-player-stats-database.zip");

    // Read and filter player data using awk
    system("awk -F, 'NR == 1 || ($3 < 25 && $8 > 85 && $9 != \"Manchester City\")  { print \"Name: \"$2; print \"Club: \"$9; print \"Age: \"$3; print \"Nationality: \"$5; print \"Potential: \"$8; print \"Photo URL: \"$4; }' FIFA23_official_data.csv");
   
    return 0;
}
