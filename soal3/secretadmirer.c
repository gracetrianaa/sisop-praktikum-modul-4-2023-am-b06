#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <openssl/sha.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>
#include <ctype.h>



#define SOURCE_DIR "/home/grace/Downloads/inifolderetc/sisop"

static const char base64_table[65] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

char* base64_encode(const unsigned char* input, int length) {
    char* encoded_data = NULL;
    int encoded_length = ((length + 2) / 3) * 4;
    encoded_data = (char*)malloc(encoded_length + 1);
    if (encoded_data == NULL)
        return NULL;

    int i, j;
    for (i = 0, j = 0; i < length; i += 3, j += 4) {
        uint32_t octet_a = i < length ? input[i] : 0;
        uint32_t octet_b = i + 1 < length ? input[i + 1] : 0;
        uint32_t octet_c = i + 2 < length ? input[i + 2] : 0;

        encoded_data[j] = base64_table[octet_a >> 2];
        encoded_data[j + 1] = base64_table[((octet_a & 3) << 4) | (octet_b >> 4)];
        encoded_data[j + 2] = base64_table[((octet_b & 15) << 2) | (octet_c >> 6)];
        encoded_data[j + 3] = base64_table[octet_c & 63];
    }

    // Padding
    int padding = length % 3;
    if (padding == 1) {
        encoded_data[encoded_length - 1] = '=';
        encoded_data[encoded_length - 2] = '=';
    } else if (padding == 2) {
        encoded_data[encoded_length - 1] = '=';
    }

    encoded_data[encoded_length] = '\0';

    return encoded_data;
}

static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];
    sprintf(fpath, "%s%s", SOURCE_DIR, path);
    res = lstat(fpath, stbuf);
    if (res == -1)
        return -errno;
    return 0;
}

static int xmp_rename(const char *oldpath, const char *newpath)
{
    int res = rename(oldpath, newpath);
    if (res == -1)
        return -errno;
    
    // printf("rename works");

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    int fd;
    int res;
    char fpath[1000];
    sprintf(fpath, "%s%s", SOURCE_DIR, path);
    (void)fi;
    fd = open(fpath, O_RDONLY);
    if (fd == -1)
        return -errno;
    res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;
    close(fd);
    return res;
}

static int xmp_open(const char* path, struct fuse_file_info* fi) {

    int res;
    char fpath[1000];
    
    sprintf(fpath, "%s%s", SOURCE_DIR, path);
    
    res = open(fpath, fi->flags);
    if (res == -1)
        return -errno;
    close(res);
    return 0;
}


static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = SOURCE_DIR;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", SOURCE_DIR, path);

    

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;
        char etc_path[2000];

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;


        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
            continue;

        
        sprintf(etc_path, "%s/%s", fpath, de->d_name);

        
        printf("%s\n", de->d_name);

        FILE *file = fopen(etc_path, "rb");
        if (file) {
            fseek(file, 0, SEEK_END);
            long file_size = ftell(file);
            fseek(file, 0, SEEK_SET);

            unsigned char *file_buffer = (unsigned char*) malloc(file_size);
            if (file_buffer) {
                fread(file_buffer, 1, file_size, file);

                if (tolower(de->d_name[0]) == 'l' || tolower(de->d_name[0]) == 'u' || tolower(de->d_name[0]) == 't' || tolower(de->d_name[0]) == 'h') {
                    char* encoded_content = base64_encode(file_buffer, file_size);
                    if (encoded_content != NULL) {
                        FILE *new_file = fopen(etc_path, "wb");
                        if (new_file) {
                            fwrite(encoded_content, 1, strlen(encoded_content), new_file);
                            fclose(new_file);
                        }
                        free(encoded_content);
                    }
                }

                free(file_buffer);
            }
            fclose(file);
        }

        size_t filename_len = strlen(de->d_name);
        if  (filename_len <= 4) {

               
            // untuk mengubah ke bienr
                char binary_name[40] = "";
                size_t name_len = strlen(de->d_name);
                for (size_t i = 0; i < name_len; i++)
                {
                    char binary_char[9] = "";
                    char c = de->d_name[i];
                    for (int j = 7; j >= 0; j--)
                    {
                        binary_char[7 - j] = ((c >> j) & 1) ? '1' : '0';
                    }
                    binary_char[8] = '\0';

                    strcat(binary_name, binary_char);
                    strcat(binary_name, " ");
                }

                binary_name[strlen(binary_name) - 1] = '\0';

                char etc_new_path[3000];
                sprintf(etc_new_path, "%s/%s", fpath, binary_name);

                if (xmp_rename(etc_path, etc_new_path) == -1)
                {
                    printf("Failed to rename %s\n", etc_path);
                    res = -errno;
                    break;
                }

                strcpy(de->d_name, binary_name);
        }else{

           
            if (S_ISDIR(st.st_mode))
            {
               
                size_t filename_len = strlen(de->d_name);
                for (size_t i = 0; i < filename_len; i++)
                {
                    if (islower(de->d_name[i]))
                        de->d_name[i] = toupper(de->d_name[i]);
                }

                
                char etc_new_path[3000];
                sprintf(etc_new_path, "%s/%s", fpath, de->d_name);

               
                if (xmp_rename(etc_path, etc_new_path) == -1) {
                    printf("Failed to rename %s\n", etc_path);
                    res = -errno;
                    break;
                }

            

            }
            else
            {
                
                size_t filename_len = strlen(de->d_name);
                for (size_t i = 0; i < filename_len; i++)
                {
                    if (isupper(de->d_name[i]))
                        de->d_name[i] = tolower(de->d_name[i]);
                }

                char etc_new_path[3000];
                sprintf(etc_new_path, "%s/%s", fpath, de->d_name);

                
                if (xmp_rename(etc_path, etc_new_path) == -1) {
                    printf("Failed to rename %s\n", etc_path);
                    res = -errno;
                    break;
                }

                
            }
        }

        res = filler(buf, de->d_name, &st, 0);
        if (res != 0)
            break;
    }

    closedir(dp);

    return res;
}





static int xmp_mkdir(const char* path, mode_t mode) {
    int res;
    char fpath[1000];
    sprintf(fpath, "%s%s", SOURCE_DIR, path);
    res = mkdir(fpath, mode);
    if (res == -1)
        return -errno;
    return 0;
}




static int xmp_write(const char* path, const char* buf, size_t size, off_t offset, struct fuse_file_info* fi) {
    int res;
    char fpath[1000];
    sprintf(fpath, "%s%s", SOURCE_DIR, path);

 
    int fd = open(fpath, O_WRONLY);
    if (fd == -1)
        return -errno;

   
    res = pwrite(fd, buf, size, offset);
    if (res == -1) {
        res = -errno;
    }

    close(fd);
    return res;
}






static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .open = xmp_open,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .write = xmp_write,
};



int  main(int  argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}