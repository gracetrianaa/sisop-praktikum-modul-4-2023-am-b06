# sisop-praktikum-modul-4-2023-AM-B06

Anggota kelompok B06 adalah :

|        Nama           | NRP|
| ---                   |--- |
|Glenaya                |5025211202 |
|Gracetriana Survinta  Septinaputri | 5025211199 |
|Ken Anargya Alkausar   | 5025211168 |

# Laporan Resmi Praktikum Modul 4 Sisop

## Soal Nomor 1
Kota Manchester sedang dilanda berita bahwa kota ini mau juara (yang kota ‘loh ya, bukan yang satunya). Tagline #YBBA (#YangBiruBiruAja #anjaykelaspepnihbossenggoldong 💪🤖🤙🔵⚪) sudah mewabah di seluruh dunia. Semua warga pun sudah menyiapkan pesta besar-besaran. 
Seorang pelatih sepak bola handal bernama Peb merupakan pelatih klub Manchester Blue, sedang berjuang memenangkan Treble Winner. Untuk meraihnya, ia perlu melakukan pembelian pemain dengan ideal. Agar sukses, ia memahami setiap detail data performa pemain sepak bola seluruh dunia yang meliputi statistik pemain, umur, tinggi dan berat badan, potensi, klub dan negaranya, serta banyak data lainnya. Namun, tantangan tersendiri muncul ketika mengelola dan mengakses data berukuran besar ini.
Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.

### Penyelesaian 1A
Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.

kaggle datasets download -d bryanb/fifa-player-stats-database

Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut.  Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya. 

```c
int main() {
    // Download dataset
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");

    // Extract dataset from the downloaded zip file
    system("unzip fifa-player-stats-database.zip");

    // Cleanup: remove the downloaded zip file
    system("rm fifa-player-stats-database.zip");

    return 0;
}
```
Code ini akan mengunduh dataset dengan menggunakan perintah kaggle dari Kaggle. Dataset yang diunduh adalah fifa-player-stats-database, kemudian akan mengekstrak dataset dari file zip yang telah diunduh. Perintah unzip digunakan untuk mengekstrak file zip. Dalam kasus ini, perintah unzip akan mengekstrak semua file yang ada di dalam file zip.Dalam kasus ini, perintah rm digunakan untuk menghapus file zip fifa-player-stats-database.zip.

### Penyelesaian 1B
Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file storage.c untuk analisis ini.

```c
int main() {
    // Read and filter player data using awk
    system("awk -F, 'NR == 1 || ($3 < 25 && $8 > 85 && $9 != \"Manchester City\")  { print \"Name: \"$2; print \"Club: \"$9; print \"Age: \"$3; print \"Nationality: \"$5; print \"Potential: \"$8; print \"Photo URL: \"$4; }' FIFA23_official_data.csv");
   
    return 0;
}
```
membaca dan memfilter data pemain dari file CSV. Mencetak informasi pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub selain "Manchester City". Informasi yang dicetak meliputi nama pemain, klub, usia, kewarganegaraan, potensi, dan URL foto.

### Penyelesaian 1C
Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal. Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan.
Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.

```dockerfile
# Gunakan base image dari distribusi Linux yang sesuai
FROM ubuntu:latest

# Update paket dan instal dependensi yang diperlukan
RUN apt-get update && apt-get install -y \
    gcc \
    unzip \
    python3-pip 

# Menginstal Kaggle CLI menggunakan pip
RUN pip3 install kaggle

# Menyalin file storage.c dan FIFA23_official_data.csv ke dalam image
COPY storage.c /

# Menyalin file kaggle.json ke dalam image
COPY kaggle.json /root/.kaggle/kaggle.json
RUN chmod 600 /root/.kaggle/kaggle.json

# Mengcompile storage.c
RUN gcc /storage.c -o /storage

# Memberikan izin eksekusi pada file storage
RUN chmod +x /storage

# Menjalankan storage saat container dijalankan
CMD ["/storage"]
```
Dockerfile ini menggunakan base image dari distribusi Linux Ubuntu terbaru. Kemudian, dilakukan pembaruan paket sistem dan instalasi dependensi yang diperlukan seperti `gcc`, `unzip`, dan `python3-pip`.Selanjutnya, dilakukan instalasi Kaggle CLI menggunakan pip3. Kaggle CLI diperlukan untuk mengunduh dataset dari Kaggle. File `storage.c` yang merupakan program yang akan dijalankan, disalin ke dalam image. File `kaggle.json` yang berisi informasi otentikasi untuk mengakses API Kaggle juga disalin ke dalam image. Kemudian, izin yang tepat diberikan pada file `kaggle.json` di dalam image untuk keamanan. Selanjutnya, program `storage.c` dikompilasi menjadi file eksekusi `/storage`, dan izin eksekusi diberikan pada file tersebut. Terakhir, diatur perintah default yang akan dijalankan saat container berjalan, yaitu menjalankan program `/storage`. Dengan menggunakan Docker CLI dan Dockerfile ini, Anda dapat membangun Docker Image dengan perintah `docker build`, dan kemudian menjalankan Docker Container menggunakan image yang telah dibuat.

### Penyelesaian 1D
Setelah sukses membuat sistem berbasis Docker, Peb merasa bahwa sistem ini tidak hanya berguna untuk dirinya sendiri, tetapi juga akan akan membantu para scouting-nya yang terpencar di seluruh dunia dalam merekrut pemain berpotensi tinggi. Namun, satu tantangan muncul, yaitu bagaimana caranya para scout dapat mengakses dan menggunakan sistem yang telah diciptakan?
Merasa terpanggil untuk membantu Peb lebih jauh, kalian memutuskan untuk mem-publish Docker Image sistem ke Docker Hub, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.
```
https://hub.docker.com/repository/docker/kenanargyaa/storage-app/general
```
![image](https://github.com/dagdo03/Word_Ladder/assets/92387421/bd57b0ad-9e16-467e-b823-a80eef234d63)

### Penyelesaian 1E
Berita tagline #YBBA (#YangBiruBiruAja) semakin populer, dan begitu juga sistem yang telah kalian buat untuk membantu Peb dalam rekrutmen pemain. Beberapa klub sepak bola lain mulai menunjukkan minat pada sistem tersebut dan permintaan penggunaan semakin bertambah. Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.
```yml
version: '3'
services:
  instance1:
    image: fifa
    ports:
      - "8081:80"

  instance2:
    image: fifa
    ports:
      - "8082:80"

  instance3:
    image: fifa
    ports:
      - "8083:80"

  instance4:
    image: fifa
    ports:
      - "8084:80"

  instance5:
    image: fifa
    ports:
      - "8085:80"
```
menjalankan beberapa instance dari Docker Container dengan menggunakan image fifa dan meneruskan lalu lintas ke dalam container melalui port yang ditentukan. Konfigurasi ini menggunakan versi 3 dari docker-compose. Terdapat beberapa services yang didefinisikan, yaitu instance1, instance2, instance3, instance4, dan instance5. Setiap instance akan menggunakan image fifa sebagai dasar untuk menjalankan Docker Container. Port-port yang ditentukan adalah 8081, 8082, 8083, 8084, dan 8085 yang akan diteruskan ke dalam port 80 di dalam container. Ini berarti instance1 akan dapat diakses melalui localhost:8081, instance2 melalui localhost:8082, dan seterusnya.

## Soal Nomor 2
Nana adalah peri kucing yang terampil dalam menggunakan sihir, dia bisa membuat orang lain berubah menjadi Molina. Suatu hari, dia bosan menggunakan sihir dan mencoba untuk magang di germa.dev sebagai programmer.
Agar dapat diterima sebagai karyawan magang dia diberi sebuah file .zip yang berisi folder dan file dari Germa. Kemudian, Nana harus membuat sistem manajemen folder dengan ketentuan sebagai berikut:
Apabila terdapat file yang mengandung kata restricted, maka file tersebut tidak dapat di-rename ataupun dihapus.
Apabila terdapat folder yang mengandung kata restricted, maka folder tersebut, folder yang ada di dalamnya, dan file yang ada di dalamnya tidak dapat di-rename ataupun dihapus.
Contoh:
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kane.txt

Karena latar belakang Nana adalah seorang penyihir, dia ingin membuat satu kata sihir yaitu bypass yang dapat menghilangkan kedua aturan tadi. Kata sihir tersebut akan berlaku untuk folder dan file yang ada di dalamnya.
Case invalid untuk bypass:
/jalan/keputih/perintis/iv/tidakrestrictedlohini/gang/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gangIV/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanelagiramai.txt
Case valid untuk bypass:
/jalan/keputih/perintis/iv/tadirestrictedtapibypasskok/gang/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kaneudahtutup.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanepakaibypass.txt

>> - Buatlah folder productMagang pada folder /src_data/germa/products/restricted_list/. Kemudian, buatlah folder projectMagang pada /src_data/germa/projects/restricted_list/. Akan tetapi, hal tersebut akan gagal.
>> - Ubahlah nama folder dari restricted_list pada folder /src_data/germa/projects/restricted_list/ menjadi /src_data/germa/projects/bypass_list/. Kemudian, buat folder projectMagang di dalamnya.
>> - Karena folder projects menjadi dapat diakses, ubahlah folder filePenting di dalam folder projects menjadi restrictedFilePenting agar secure kembali. Coba keamanannya dengan mengubah nama file yang ada di dalamnya.
>> - Terakhir, coba kamu hapus semua fileLama di dalam folder restrictedFileLama. Jangan lupa untuk menambahkan kata sihir pada folder tersebut agar folder tersebut dapat terhapus
>> - Semua kegiatan yang kalian lakukan harus tercatat dalam sebuah file logmucatatsini.txt di luar file .zip yang diberikan kepada Nana, agar pekerjaan yang kamu lakukan dapat dilaporkan kepada Germa. 
Adapun format logging yang harus kamu buat adalah sebagai berikut. 
[STATUS]::[dd]/[MM]/[yyyy]-[HH]:[mm]:[ss]::[CMD]::[DESC]
Dengan keterangan sebagai berikut.
STATUS: SUCCESS atau FAILED.
dd: 2 digit tanggal.
MM: 2 digit bulan.
yyyy: 4 digit tahun.
HH: 24-hour clock hour, with a leading 0 (e.g. 22).
mm: 2 digit menit.
ss: 2 digit detik.
CMD: command yang digunakan (MKDIR atau RENAME atau RMDIR atau RMFILE atau lainnya).
DESC: keterangan action, yaitu:
[User]-Create directory x.
[User]-Rename from x to y.
[User]-Remove directory x.
[User]-Remove file x

### Pembahasan file germa.c
```c
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <time.h>
#include <pwd.h>

static const char *dirpath = "/home/glenaya/Sisop/prak4/nanaxgerma/nanaxgerma/src_data";
static const char *logpath = "/home/glenaya/Sisop/prak4/logmucatatsini.txt";

static int xmp_mkdir(const char *path, mode_t mode) {
    int res;
	
	char finalpath[2000];
	sprintf(finalpath, "%s%s", dirpath, path);

    struct passwd *pw = getpwuid(getuid());
    const char *user = pw ? pw->pw_name : "unknown";
	
	if (strstr(path, "restricted") != NULL && strstr(path, "bypass") == NULL) {
		FILE *logFile = fopen(logpath, "a");
	    if (logFile != NULL) {
	        time_t t = time(NULL);
	        struct tm *tm_info = localtime(&t);
	        char timestamp[26];
	        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
	        
	        fprintf(logFile, "FAILED::%s::MKDIR::[%s] Create %s\n", timestamp, user, finalpath);
	        
	        fclose(logFile);
	    }
	} else {
		res = mkdir(finalpath, mode);
	    FILE *logFile = fopen(logpath, "a");
	    if (logFile != NULL) {
	        time_t t = time(NULL);
	        struct tm *tm_info = localtime(&t);
	        char timestamp[26];
	        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
	
	        fprintf(logFile, "SUCCESS::%s::MKDIR::[%s] Create %s\n", timestamp, user, finalpath);
	
	        fclose(logFile);
	    }
	    if (res == -1) return -errno;
	}	
    return 0;
}
```

- Pada awalnya, kita memasukan semua library yang akan digunakan. Lalu path dideklarasi untuk source mount serta sebagai path untuk file log.txt.
- Langkah adalah untuk membuat fungsi mkdir. Pertama source path dan path input akan digabungkan untuk mendapatkan final path. Lalu, informasi pengguna akan didapat menggunakan getUID. Selanjutnya, kita akan memeriksa apakah path tersebut mengandung kata ```restricted``` dan tidak mengandung kata ```bypass```. Jika demikian, maka itu berarti tidak dapat membuat direktori baru ```(mkdir)``` dan akan menulis ```failed``` ke dalam log. Ketika menulis log, kita akan mendapatkan waktu saat ini menggunakan time. Selanjutnya, jika tidak ada kata ```restricted``` maka ```mkdir``` akan diizinkan untuk dilakukan dan akan menulis ```success``` pada log.

```c
static int xmp_rename(const char *oldpath, const char *newpath) {
    int res;
	
	char foldpath[2000];
	sprintf(foldpath, "%s%s", dirpath, oldpath);
	
	char fnewpath[2000];
	sprintf(fnewpath, "%s%s", dirpath, newpath);
	
	char* resPosition = strstr(foldpath, "restricted");
	char* pasPosition = strstr(fnewpath, "bypass");
	
    struct passwd *pw = getpwuid(getuid());
    const char *user = pw ? pw->pw_name : "unknown";
	
    if (strstr(foldpath, "restricted") != NULL && strstr(foldpath, "bypass") == NULL) {
        if (strstr(fnewpath, "bypass") == NULL) {
        	
            fprintf(stderr, "Error: Renaming a restricted file or folder is not allowed.\n");
            
			FILE *logFile = fopen(logpath, "a");
		    if (logFile != NULL) {
		        time_t t = time(NULL);
		        struct tm *tm_info = localtime(&t);
		        char timestamp[26];
		        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
		
		        fprintf(logFile, "FAILED::%s::RENAME::[%s] Rename from %s to %s\n", timestamp, user, foldpath, fnewpath);
		
		        fclose(logFile);
		    }
            return -EINVAL;
        }else {
        	res = rename(foldpath, fnewpath);
        	
        	FILE *logFile = fopen(logpath, "a");
		    if (logFile != NULL) {
		        time_t t = time(NULL);
		        struct tm *tm_info = localtime(&t);
		        char timestamp[26];
		        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
		
		        fprintf(logFile, "SUCCESS::%s::RENAME::[%s] Rename from %s to %s\n", timestamp, user, foldpath, fnewpath);
		
		        fclose(logFile);
		    }
		    if (res == -1) return -errno;
		}
    }else if(strstr(foldpath, "restricted") != NULL && strstr(foldpath, "bypass") != NULL) {
        if(resPosition > pasPosition){
			
			FILE *logFile = fopen(logpath, "a");
		    if (logFile != NULL) {
		        time_t t = time(NULL);
		        struct tm *tm_info = localtime(&t);
		        char timestamp[26];
		        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
		
		        fprintf(logFile, "FAILED::%s::RENAME::[%s] Rename from %s to %s\n", timestamp, user, foldpath, fnewpath);
		
		        fclose(logFile);
		    }
            return -EINVAL;
		}else {
			res = rename(foldpath, fnewpath);
    
        	FILE *logFile = fopen(logpath, "a");
		    if (logFile != NULL) {
		        time_t t = time(NULL);
		        struct tm *tm_info = localtime(&t);
		        char timestamp[26];
		        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
		
		        fprintf(logFile, "SUCCESS::%s::RENAME::[%s] Rename from %s to %s\n", timestamp, user, foldpath, fnewpath);
		
		        fclose(logFile);
		    }
		    if (res == -1) return -errno;
		}
	}else {
    	res = rename(foldpath, fnewpath);

    	FILE *logFile = fopen(logpath, "a");
	    if (logFile != NULL) {
	        time_t t = time(NULL);
	        struct tm *tm_info = localtime(&t);
	        char timestamp[26];
	        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
		
	        fprintf(logFile, "SUCCESS::%s::RENAME::[%s] Rename from %s to %s\n", timestamp, user, foldpath, fnewpath);
		
	        fclose(logFile);
		}
	    if (res == -1) return -errno;
	}
    return 0;
}
```
Fungsi di atas merupakan ```xmp_rename``` yang berguna untuk rename direktori.
- Pertama, kita akan menggabungkan oldpath dan newpath. 
- Kedua, kita akan mendapatkan informasi pengguna dengan menggunakan getUID dan (jika ada) lokasi kata "restricted" dan "bypass". Setelah itu, kita akan memeriksa path. Jika pada oldpath terdapat kata "restricted" dan pada newpath tidak terdapat kata "bypass", maka akan dianggap gagal ```(failed)```. Namun, jika tidak, maka proses rename akan berhasil ```(success)```. Selanjutnya, kita akan memeriksa jika pada newpath tidak terdapat kata ```bypass``` dan pada oldpath terdapat kata ```restricted``` dan kata ```bypass```, maka kita akan memeriksa lokasi kata ```restricted``` dan ```bypass```
- Jika kata ```restricted```lebih dalam dari kata ```bypass```, maka akan dianggap gagal karena file yang ```restricted``` berada lebih dalam daripada ```bypass```. Jika tidak, maka proses rename akan berhasil ```(success)```. Selanjutnya, jika semua kondisi di atas tidak terpenuhi, berarti tidak ada kata ```restricted```, sehingga proses rename akan berhasil ```(success)```.

```c
static int xmp_unlink(const char *path) {
    int res;
    
    char finalpath[2000];
	sprintf(finalpath, "%s%s", dirpath, path);
    
    struct passwd *pw = getpwuid(getuid());
    const char *user = pw ? pw->pw_name : "unknown";
    
    struct stat st;
    if (lstat(finalpath, &st) == -1) return -errno;

    const char *function;
    const char *function2;
    if (S_ISDIR(st.st_mode)) {
        function = "RMDIR";
        function2 = "directory";
    }else {
        function = "RMFILE";
        function2 = "file";
	}
	
    if (strstr(path, "restricted") != NULL && strstr(path, "bypass") == NULL) {
        fprintf(stderr, "Error: Deleting a restricted file or folder is not allowed.\n");
        
		FILE *logFile = fopen(logpath, "a");
	    if (logFile != NULL) {
	        time_t t = time(NULL);
	        struct tm *tm_info = localtime(&t);
	        char timestamp[26];
	        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
	
	        fprintf(logFile, "FAILED::%s::%s::[%s] Remove %s %s\n", timestamp, function, user, function2, finalpath);
	
	        fclose(logFile);
	    }
		return -EINVAL;
    }else { 
    	res = unlink(finalpath);
    	
    	FILE *logFile = fopen(logpath, "a");
	    if (logFile != NULL) {
	        time_t t = time(NULL);
	        struct tm *tm_info = localtime(&t);
	        char timestamp[26];
	        strftime(timestamp, sizeof(timestamp), "%Y/%m/%d-%H:%M:%S", tm_info);
	
	        fprintf(logFile, "SUCCESS::%s::%s::[%s] Remove %s %s\n", timestamp, function, user, function2, finalpath);
	
	        fclose(logFile);
	    }
    	if (res == -1) return -errno;
	}

    return 0;
}
```
- Fungsi di atas bernama ```xmp_unlink``` berguna untuk remove file dan folder.
- Pertama kita akan menggabungkan path.
- Kemudian, kita akan mendapatkan informasi pengguna. 
- Selanjutnya, kita akan memeriksa apakah yang akan dihapus adalah file atau direktori, sehingga dapat memformatnya dengan benar saat menulis log.
- Selanjutnya, kita akan memeriksa path. Jika terdapat kata ```restricted``` dan tidak ada kata ```bypass```, maka akan dianggap gagal ```(failed)```. Namun, jika tidak, maka proses penghapusan akan berhasil ```(success)```.

```c
static int xmp_getattr(const char *path, struct stat *stbuf) {
    int res;
    
    char finalpath[2000];
	sprintf(finalpath, "%s%s", dirpath, path);

    res = lstat(finalpath, stbuf);
    if (res == -1)
        return -errno;

    return 0;
}
```
Fungsi di atas bernama ```xmp_getattr``` berguna untuk read dimana meggabungkan path lalu akan di-_read_.

```c
static struct fuse_operations xmp_oper = {
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .unlink = xmp_unlink,
    .getattr = xmp_getattr,
};

int main(int argc, char *argv[]) {
	
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
    
}
```
Kode di atas adalah ```struct``` untuk mendefinisikan operasi yang diimplementasi pada program FUSE ini dan ```main``` yang merupakan fungsi utama program.

### Hasil logmucatatsini.txt
Ini merupakan apa yang diminta pada soal dan hasil log nya sebagian adalah sebagai berikut :

```txt
FAILED::2023/06/03-16:59:47::MKDIR::[glenaya] Create /home/glenaya/Sisop/prak4/nanaxgerma/nanaxgerma/src_data/germaa/products/restricted_list/productMagang
FAILED::2023/06/03-17:02:49::MKDIR::[glenaya] Create /home/glenaya/Sisop/prak4/nanaxgerma/nanaxgerma/src_data/germaa/projects/restricted_list/projectMagang
SUCCESS::2023/06/03-17:03:52::RENAME::[glenaya] Rename from /home/glenaya/Sisop/prak4/nanaxgerma/nanaxgerma/src_data/germaa/projects/restricted_list to /home/glenaya/Sisop/prak4/nanaxgerma/nanaxgerma/src_data/germaa/projects/bypass_list
SUCCESS::2023/06/03-17:08:13::MKDIR::[glenaya] Create /home/glenaya/Sisop/prak4/nanaxgerma/nanaxgerma/src_data/germaa/projects/bypass_list/projectMagang
SUCCESS::2023/06/03-17:09:26::RENAME::[glenaya] Rename from /home/glenaya/Sisop/prak4/nanaxgerma/nanaxgerma/src_data/germaa/projects/bypass_list/filePenting to /home/glenaya/Sisop/prak4/nanaxgerma/nanaxgerma/src_data/germaa/projects/bypass_list/restrictedFilePenting

```
## Soal Nomor 3 
Dhafin adalah seorang yang sangat pemalu, tetapi saat ini dia sedang mengagumi seseorang yang dia tidak ingin seorangpun tahu. Maka dari itu, dia melakukan berbagai hal sebagai berikut.

### Penyelesaian 3A
> Dhafin ingin membuat sebuah FUSE yang termodifikasi dengan source mount-nya adalah /etc yang bernama secretadmirer.c. 

Pertama-tama, dilakukan definisi versi fuse yang digunakan, semua library yang akan digunakan pada program, serta `SOURCE_DIR` yang berisi path source directory yang akan dimount.

```c
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <openssl/sha.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>
#include <ctype.h>


#define SOURCE_DIR "/home/grace/Downloads/inifolderetc/sisop"
```

```secretadmirer.c

static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];
    sprintf(fpath, "%s%s", SOURCE_DIR, path);
    res = lstat(fpath, stbuf);
    if (res == -1)
        return -errno;
    return 0;
}

static int xmp_rename(const char *oldpath, const char *newpath)
{
    int res = rename(oldpath, newpath);
    if (res == -1)
        return -errno;
    
    // printf("rename works");

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    int fd;
    int res;
    char fpath[1000];
    sprintf(fpath, "%s%s", SOURCE_DIR, path);
    (void)fi;
    fd = open(fpath, O_RDONLY);
    if (fd == -1)
        return -errno;
    res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;
    close(fd);
    return res;
}

static int xmp_open(const char* path, struct fuse_file_info* fi) {

    int res;
    char fpath[1000];
    
    sprintf(fpath, "%s%s", SOURCE_DIR, path);
    
    res = open(fpath, fi->flags);
    if (res == -1)
        return -errno;
    close(res);
    return 0;
}

. . .
. . .

static int xmp_mkdir(const char* path, mode_t mode) {
    int res;
    char fpath[1000];
    sprintf(fpath, "%s%s", SOURCE_DIR, path);
    res = mkdir(fpath, mode);
    if (res == -1)
        return -errno;
    return 0;
}




static int xmp_write(const char* path, const char* buf, size_t size, off_t offset, struct fuse_file_info* fi) {
    int res;
    char fpath[1000];
    sprintf(fpath, "%s%s", SOURCE_DIR, path);

 
    int fd = open(fpath, O_WRONLY);
    if (fd == -1)
        return -errno;

   
    res = pwrite(fd, buf, size, offset);
    if (res == -1) {
        res = -errno;
    }

    close(fd);
    return res;

}


static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .open = xmp_open,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .write = xmp_write,
};

```
Fungsi-fungsi di atas merupakan bagian dari FUSE file system operations yang memiliki cara bekerja mirip dengan UNIX file system operations. Fungsi tersebut akan dipanggil oleh pointer yang disimpan di dalam sebuah struct `fuse_operations`. Di dalam tiap fungsi kurang lebih memiliki isi kode yang sama, digunakan `sprintf` untuk menyimpan path baru dari `SOURCE_DIR` dan string `path` kemudian res akan menyimpan operasi yang akan dilakukan sesuai fungsi tersebut (open, mkdir, dsb.). Jika terjadi error `(res == -1)`, fungsi akan langsung mengembalikan nilai negatif dari `errno`. Berikut adalah beberapa fungsi yang digunakan pada program ini:
- Fungsi `xmp_getattr` : dipanggil saat sistem mencoba untuk mendapatkan attribut dari sebuah file.
- Fungsi `xmp_readdir` : dipanggil saat user mencoba untuk menampilkan file dan direktori yang berada pada suatu direktori yang spesifik.
- Fungsi `xmp_read` : dipanggil saat sistem membaca potongan demi potongan data dalam sebuah file.
- Fungsi `xmp_open` : dipanggil saat sistem membuka file dari file system FUSE.
- Fungsi `xmp_mkdir` : dipanggil saat user membuat direktori baru dalam sistem.
- Fungsi `xmp_rename` : dipanggil saat user hendak mengganti nama file atau memindah file.
- Fungsi `xmp_write` : dipanggil saat sistem menulis data ke dalam file. Pada implementasinya, fungsi ini akan membuka file dengan menggunakan path yang ditentukan dan menggunakan fungsi pwrite untuk menulis data ke dalam file.

```c

int  main(int  argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}
```
Di dalam fungsi main ini dilakukan pemanggilan untuk menjalankan semua fungsi FUSE operations yang telah ditulis. `umask(0)` digunakan untuk mengatur nilai umask. Dalam kode ini, `umask(0)` mengatur umask menjadi 0, yang berarti tidak ada perubahan pada hak akses yang ditetapkan saat membuat file atau direktori. Dengan nilai umask 0, semua hak akses yang ditentukan dalam kode FUSE akan diterapkan sepenuhnya. `return fuse_main` menerima argumen dari `argc`, `argv`, struct `xmp_oper`, dan nilai `NULL` yang menandakan tidak ada argumen khusus yang diteruskan ke oeprasi FUSE. Setelah menjalankan `fuse_main`, program ini akan memulai proses mounting file system FUSE dan menjalankan file system FUSE tersebut.

### Penyelesaian 3B
> Lalu, untuk menutupi rahasianya, Dhafin ingin semua direktori dan file yang berada pada direktori yang diawali dengan huruf L, U, T, dan H akan ter-encode dengan Base64.  Encode berlaku rekursif (berlaku untuk direktori dan file yang baru di-rename atau pun yang baru dibuat).

```c
static const char base64_table[65] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

char* base64_encode(const unsigned char* input, int length) {
    char* encoded_data = NULL;
    int encoded_length = ((length + 2) / 3) * 4;
    encoded_data = (char*)malloc(encoded_length + 1);
    if (encoded_data == NULL)
        return NULL;

    int i, j;
    for (i = 0, j = 0; i < length; i += 3, j += 4) {
        uint32_t octet_a = i < length ? input[i] : 0;
        uint32_t octet_b = i + 1 < length ? input[i + 1] : 0;
        uint32_t octet_c = i + 2 < length ? input[i + 2] : 0;

        encoded_data[j] = base64_table[octet_a >> 2];
        encoded_data[j + 1] = base64_table[((octet_a & 3) << 4) | (octet_b >> 4)];
        encoded_data[j + 2] = base64_table[((octet_b & 15) << 2) | (octet_c >> 6)];
        encoded_data[j + 3] = base64_table[octet_c & 63];
    }

    // Padding
    int padding = length % 3;
    if (padding == 1) {
        encoded_data[encoded_length - 1] = '=';
        encoded_data[encoded_length - 2] = '=';
    } else if (padding == 2) {
        encoded_data[encoded_length - 1] = '=';
    }

    encoded_data[encoded_length] = '\0';

    return encoded_data;
}
```
Fungsi di atas merupakan implementasi dari Base64 encode yang digunakan untuk mengenkripsi data menggunakan metode Base64. `static const char base64_table[65]` adalah variabel array base64_table yang berisi karakter-karakter yang digunakan dalam encoding Base64 dan terdiri dari karakter huruf kapital A-Z, huruf kecil a-z, angka 0-9, dan dua karakter khusus "+/". `base64_table` ini akan digunakan dan dipanggil dalam fungsi `base64_encode`.

Fungsi `base64_encode` menerima dua argumen, yaitu input yang merupakan pointer ke data yang akan dikodekan dalam bentuk `unsigned char` dan `length` yang merupakan panjang data tersebut. `int encoded_length = ((length + 2) / 3) * 4` menghitung panjang data yang akan dikodekan dalam bentuk Base64. Rumusnya adalah ((panjang data + 2) / 3) * 4, karena setiap 3 byte data akan dikodekan menjadi 4 karakter Base64.  

`encoded data = (char*)malloc(encoded_length + 1)` membuat buffer `encoded_data` dengan ukuran yang cukup untuk menyimpan hasil encoding Base64. Ukurannya adalah encoded_length + 1 untuk menyertakan karakter terminasi string '\0'. Kemudian menggunakan for loop, iterasi dilakukan untuk setiap blok data sepanjang 3 byte. Setiap blok data akan dikodekan menjadi 4 karakter Base64 sesuai dengan aturan encoding Base64. Selanjutnya, menambahkan karakter padding "=" jika panjang data tidak habis dibagi 3. Jumlah padding tergantung pada sisa pembagian dengan 3. Menambahkan karakter terminasi string '\0' pada akhir `encoded_data`. Mengembalikan pointer ke `encoded_data` yang berisi data yang telah dikodekan.

```c
...
  FILE *file = fopen(etc_path, "rb");
        if (file) {
            fseek(file, 0, SEEK_END);
            long file_size = ftell(file);
            fseek(file, 0, SEEK_SET);

            unsigned char *file_buffer = (unsigned char*) malloc(file_size);
            if (file_buffer) {
                fread(file_buffer, 1, file_size, file);

                if (tolower(de->d_name[0]) == 'l' || tolower(de->d_name[0]) == 'u' || tolower(de->d_name[0]) == 't' || tolower(de->d_name[0]) == 'h') {
                    char* encoded_content = base64_encode(file_buffer, file_size);
                    if (encoded_content != NULL) {
                        FILE *new_file = fopen(etc_path, "wb");
                        if (new_file) {
                            fwrite(encoded_content, 1, strlen(encoded_content), new_file);
                            fclose(new_file);
                        }
                        free(encoded_content);
                    }
                }

                free(file_buffer);
            }
            fclose(file);
        }
...
...
```
Potongan kode tersebut membuka file dengan mode `"rb"` (read binary) menggunakan `fopen` dan melakukan beberapa operasi pada file tersebut. Pertama, kode menghitung ukuran file dengan menggunakan `fseek` dan `ftell`. Kemudian, kode mengalokasikan buffer `file_buffer` dengan ukuran yang sama dengan ukuran file. Setelah itu, kode membaca isi file ke dalam `file_buffer` menggunakan `fread`. Setelah data file dibaca, kode melakukan pengecekan terhadap karakter pertama `de->d_name (nama file/direktori)` dan jika karakter pertama adalah `'l', 'u', 't', atau 'h'`, maka data file tersebut dienkripsi menjadi Base64 menggunakan fungsi `base64_encode`. Hasil encoding disimpan dalam `encoded_content`. Jika `encoded_content` tidak null, kode membuka file baru dengan mode `"wb"` (write binary) menggunakan `fopen`, menulis `encoded_content` ke dalam file baru menggunakan `fwrite`, kemudian menutup file baru dengan `fclose`. Terakhir, buffer `encoded_content` dan `file_buffer` yang telah dialokasikan di atasnya dibebaskan dengan `free`.

### Penyelesaian 3C
> Untuk membedakan antara file dan direktori, Dhafin ingin semua format penamaan file menggunakan lowercase sedangkan untuk direktori semua harus uppercase. Untuk nama file atau direktori yang hurufnya kurang dari atau sama dengan empat, maka ubah namanya menggunakan data format binary yang didapat dari ASCII code masing-masing karakter.

Untuk memenuhi penyelesaian soal di atas, dilakukan perubahan/penambahan kode pada `xmp_readdir`

```c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = SOURCE_DIR;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", SOURCE_DIR, path);

    

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;
        char etc_path[2000];

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;


        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
            continue;

        
        sprintf(etc_path, "%s/%s", fpath, de->d_name);

        
        printf("%s\n", de->d_name);

        ...
        // kode encode base 64 telah dijelaskan sebelumnya
        ... 

        size_t filename_len = strlen(de->d_name);
        if  (filename_len <= 4) {

               
            // untuk mengubah ke bienr
                char binary_name[40] = "";
                size_t name_len = strlen(de->d_name);
                for (size_t i = 0; i < name_len; i++)
                {
                    char binary_char[9] = "";
                    char c = de->d_name[i];
                    for (int j = 7; j >= 0; j--)
                    {
                        binary_char[7 - j] = ((c >> j) & 1) ? '1' : '0';
                    }
                    binary_char[8] = '\0';

                    strcat(binary_name, binary_char);
                    strcat(binary_name, " ");
                }

                binary_name[strlen(binary_name) - 1] = '\0';

                char etc_new_path[3000];
                sprintf(etc_new_path, "%s/%s", fpath, binary_name);

                if (xmp_rename(etc_path, etc_new_path) == -1)
                {
                    printf("Failed to rename %s\n", etc_path);
                    res = -errno;
                    break;
                }

                strcpy(de->d_name, binary_name);
        }else{

           
            if (S_ISDIR(st.st_mode))
            {
               
                size_t filename_len = strlen(de->d_name);
                for (size_t i = 0; i < filename_len; i++)
                {
                    if (islower(de->d_name[i]))
                        de->d_name[i] = toupper(de->d_name[i]);
                }

                
                char etc_new_path[3000];
                sprintf(etc_new_path, "%s/%s", fpath, de->d_name);

               
                if (xmp_rename(etc_path, etc_new_path) == -1) {
                    printf("Failed to rename %s\n", etc_path);
                    res = -errno;
                    break;
                }

            

            }
            else
            {
                
                size_t filename_len = strlen(de->d_name);
                for (size_t i = 0; i < filename_len; i++)
                {
                    if (isupper(de->d_name[i]))
                        de->d_name[i] = tolower(de->d_name[i]);
                }

                char etc_new_path[3000];
                sprintf(etc_new_path, "%s/%s", fpath, de->d_name);

                
                if (xmp_rename(etc_path, etc_new_path) == -1) {
                    printf("Failed to rename %s\n", etc_path);
                    res = -errno;
                    break;
                }

                
            }
        }

        res = filler(buf, de->d_name, &st, 0);
        if (res != 0)
            break;
    }

    closedir(dp);

    return res;
}
```

Pertama, path direktori yang diberikan diubah menjadi path lengkap dengan menggabungkannya dengan `SOURCE_DIR` menggunakan `sprintf`. Selanjutnya, direktori dengan path tersebut dibuka menggunakan `opendir`. Jika gagal membuka direktori, kode mengembalikan kode error `-errno`.

Selanjutnya, kode melakukan loop untuk membaca setiap entri di dalam direktori menggunakan `readdir`. Pada setiap entri, kode membuat path lengkap untuk entri tersebut menggunakan `sprintf` dengan memadukan path direktori dan nama entri. Selanjutnya, kode memeriksa panjang nama entri. Jika panjang nama kurang dari atau sama dengan 4 karakter, maka nama entri akan diubah menjadi representasi biner. Hal ini dilakukan dengan menggantikan setiap karakter dalam nama dengan representasi biner 8 bit. Representasi biner tersebut kemudian digunakan sebagai nama baru dan entri direname menggunakan `xmp_rename`. Jika panjang nama lebih dari 4 karakter, kode melakukan transformasi pada nama entri.

Jika entri adalah direktori, maka setiap karakter dalam nama entri akan diubah menjadi huruf kapital menggunakan `toupper`. Jika entri bukan direktori, maka setiap karakter dalam nama entri akan diubah menjadi huruf kecil menggunakan `tolower`. Setelah transformasi selesai, entri direname dengan nama baru yang telah ditransformasi menggunakan `xmp_rename`.

Terakhir, kode memanggil fungsi `filler` untuk setiap entri dalam direktori yang berhasil dibaca. Fungsi `filler` mengisi data entri tersebut ke dalam buffer yang diberikan. Jika ada kesalahan saat memanggil `filler`, kode akan menghentikan loop dan mengembalikan kode error.

Setelah selesai membaca semua entri dalam direktori, direktori ditutup dengan `closedir` dan kode mengembalikan nilai `res` yang merepresentasikan hasil operasi direktori (0 jika sukses, atau kode error jika gagal).

### Penyelesaian 3D
### Penyelesaian 3E
### Penyelesaian 3F

### Kendala
- Kendala terdapat pada nomor 3B. Kode tidak berhasil dilakukan untuk mengganti isi file dengan data yang telah terenkripsi menggunakan base64 melainkan nama file yang terenkripsi.
- Nomor 3 D, E, F tidak berhasil terselesaikan

